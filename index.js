const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose')
const routes = require('./routes/api');

// set up express app
const app = express();

// connect to mongo db
mongoose.connect('mongodb://localhost/ninjago', { useMongoClient: true });
mongoose.Promise = global.Promise;

app.use(express.static('public'));

// configure the app to use bodyParser()
app.use(bodyParser.json());

// initialize routes
app.use('/api', routes);

// error handling middleware
app.use(function(err, req, res, next){
	res.status(422).send({
		error: err._message,
		message: err.errors.name.message
	});
});

// listen for requests
app.listen(process.env.port || 3000, function(){
	console.log('now listening for requests');
});